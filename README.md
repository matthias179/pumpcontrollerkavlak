# Kavlak Pump Controller
31st August 2023, Matthias Glatt

## Hardware
* Adafruit Feather M0
* Adafruit real time clock DS3231, part no. 3013
* Adafruit Ethernet FeatherWing, SPI, CS Pin 10
* Adafruit I2C FRAM Breakout, part no. 1895
* 4x PT1000 DIN B EE461-TP4K0.5 temperature sensor
* 4x Adafruit Pt1000 temperature amplifier, part no. 3648

## Connections
* Relay output: switches the pump Pin 12 
* LED-Pin: Pin 13
* Connector X1:
    * I2C bus (SCL, SDA)
    * SPI bus (MOSI, MISO, SCK)
    * Digital Out 1: SPI CS, Temperature forward
    * Digital Out 2: SPI CS, Temperature backward
    * Digital Out 3: SPI CS, Temperature tank
    * Digital Out 4: SPI CS, Temperature sun

## Programming Software


## Function
The software module provides three function modes:
* Temperature mode: the pump is switched on and off based on the temperatures measured. Parameters:
    * Minimum Temperature difference between forward and backward temperature measured
    * OFF delay: when OFF criteria is reached (min. temperature difference), wait this long before switching the pump off.
    * Sun temperature: no, that's not the astronomical temperature of the sun.. but a temperature sensor that should detect if the sun is shining or not. 
    * Time: switch pump never on after this time
    * Time: switch pump never on before this time
* Timer mode: the pump is switched on and off based on time settings (same for every day). Parameters:
    * Time pump on
    * Time pump off
* Manual mode: the pump is switched on and off manually



# Developer notes

## Deployment


## Website index.html file
The website's index.html file is transferred to the Arduino / Controllino with the
python script website2fram.py. This will gzip-compress the file and store the 
compressed index.html data on the Controllion's external fram.
Changing SVG in inkscape: save as optimized svg, then manually edit / shorten.


## Hints
PlatformIO:
Changing project path makes problems:
- in .vscode subfolder some json files store absolute paths -> change manually.
- sometimes wrong project is compiled. Remove all other projects from vscode workspace.

SD Card:
An attempt to load the website from SD Card (Adafruit SD Card interface) failed.
It seems that ethernet and sd card interface (both on SPI-bus) interefere somehow,
so that they can not work together!

Strings:
Global Strings also made problems, e.g. for saving and passing on a HTTP request.
Try to avoid using Strings wherever possible.

