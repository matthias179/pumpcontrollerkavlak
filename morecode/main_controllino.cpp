/*
Controllino Pump controller for Kavlak.
Steuert die Umwälzpumpe, die das Wasser über den Schlauch auf dem Dach leitet, so dass
das Wasser bestmöglich geheizt wird. Das Wasser sollte also nur fliessen,
wenn die Sonne wärmt.
Funktionen:
- 4 Temperatur-Kanäle, per SPI Bus
- 1 Relais für die Pumpe (R0)
- Externe Real time clock mit Batterie-Buffer, per I2C Bus
- FRAM für Website, per I2C Bus, Adresse 0x50
- Webserver zum Anzeigen und einstellen der Werte / Schaltverhalten
- Einstellungen werden im Controllino EEPROM gespeichert

Quellen:
https://www.controllino.com
https://github.com/adafruit/RTClib
https://github.com/pimylifeup/Arduino-Web-Server/blob/master/AJAX/AJAX.ino
https://learn.adafruit.com/adafruit-i2c-fram-breakout/overview
*/


/*
CS (SPI Chip Select):
DO1: Temp. 1
DO2: Temp. 2
DO3: Temp. 3
DO4: Temp. 4
*/


// #include <Controllino.h>
#include <EEPROM.h>
#include <Wire.h>
#include <SPI.h>
#include <Ethernet.h>
#include <ArduinoJson.h>
#include <EveryTimer.h>
#include <RTClib.h>
#include <Adafruit_MAX31865.h>
#include "Adafruit_FRAM_I2C.h"



// Mode fürs raufladen der Website. Auf 1 setzen, um die Website ins FRAM zu laden.
#define WEBSITE_TO_FRAM_MODE 0

// Wert für Ungültige Werte (defekte Sensoren usw.)
#define INVALID -999

// Pinbelegung: 
// Pumpe
#define PUMP_OUT_PIN CONTROLLINO_R0

// Aktivitäten-Anzeige LEDs
#define LED_ACTIVITY_PIN CONTROLLINO_DO0
#define LED_ETHERNET_ACTIVE_PIN CONTROLLINO_DO6

// CS-Pin für SPI-Bus
#define SPI_CS_TEMP1 CONTROLLINO_PIN_HEADER_DIGITAL_OUT_04
#define SPI_CS_TEMP2 CONTROLLINO_PIN_HEADER_DIGITAL_OUT_03
#define SPI_CS_TEMP3 CONTROLLINO_PIN_HEADER_DIGITAL_OUT_02
#define SPI_CS_TEMP4 CONTROLLINO_PIN_HEADER_DIGITAL_OUT_01

// Fortschritts LED's fürs Aufstarten
#define LED_PROGRESS_STEP1 CONTROLLINO_DO6
#define LED_PROGRESS_STEP2 CONTROLLINO_DO7
#define LED_PROGRESS_STEP3 CONTROLLINO_DI0
#define LED_PROGRESS_STEP4 CONTROLLINO_DI1
#define LED_PROGRESS_STEP5 CONTROLLINO_DI2
#define LED_PROGRESS_STEP6 CONTROLLINO_DI3

// EEPROM Adressen
#define EE_HASDATA_ADDR 0
#define EE_SETTINGS_ADDR 1


// Update Daten und Status
#define UPDATE_INTERVAL 5000


// Struktur Definitionen
// Zeit-Werte immer als "Dezimal-Zeit": hour*100+minute. Datum und Sekunden werden ignoriert.
// Einstellungen
struct SettingStruct {
  unsigned int pumpMode;      // Mode: 0: manual, 1: time, 2: temperature
  unsigned int pumpOnTime;    // time mode: Zeit Pumpe ein
  unsigned int pumpOffTime;   // time mode: Zeit Pumpe aus
  float deltaTemp;            // temperature mode: Differenz zwischen vor- und rücklauf
  unsigned int pumpOffDelay;  // temperature mode: Verzögerung beim ausschalten der Pumpe
  float lockSunTemp;          // temperature mode: Schwelltemperatur für Sonne scheint
  unsigned int lockStartTime; // temperature mode: Pumpe auf jeden Fall aus Start-Zeit
  unsigned int lockStopTime;  // temperature mode: Pumpe auf jeden Fall aus Ende-Zeit
};

// Standard-Einstellungen
SettingStruct Settings = {
  2,      // Temperature mode
  900,    // Pumpe ein 9:00 Uhr
  2000,   // Pumpe aus 20:00 Uhr
  5,      // Differenztemperatur 5°C
  5,      // Pumpe aus Verzögerung 5 min.
  60,     // Sonne scheint bei 60°C
  2000,   // Pumpe nie ein nach 20:00 Uhr
  900};   // Pumpe nie ein vor 9:00 Uhr


// Aktuelle Daten
struct StateStruct {
  char timeStamp[20];     // Zeit als string: 2023-08-31T01:02:03, wann die Daten eingelesen wurden
  float tempFwd;          // Vorlauf-Temperatur
  float tempBack;         // Rücklauf-Temperatur
  float tempTank;         // Temperatur im Tank (nur Anzeige, keine Schaltfunktion)
  float tempSun;          // Temperatur Sonnen-Sensor
  unsigned int pumpState; // Zustand Pumpe (0: aus, 1: ein)
  unsigned int sunState;  // Sonne scheint?
  String message;         // Meldungen
};
StateStruct StateData = {"",INVALID,INVALID,INVALID,INVALID,0,0, ""};


// Timer, aktualisiert den State jede Sekunde
EveryTimer stateUpdateTimer;

// Real time clock (nicht von Controllino, sondern externe
// mit Batterie-Buffer)
RTC_DS3231 rtcClock;

// Haben sich die Einstellungen geändert und müssen ins EEPROM gespeichert werden?
bool eeSettingsChanged = false;

// Einstellungen mit den Daten an den Web Client mitsenden?
bool webSendSettings = true;

// Ausschaltverzögerung für die Pumpe: enthält den millis() Zeitstempel,
// wann die Ausschaltverzögerung abgelaufen ist. Oder 0, wenn die
// Ausschaltverzögerung nicht läuft.
unsigned long pumpOffDelayEnds = 0;



// Webserver ==========================================
// Hier die MAC Adresse des Shields eingeben
// (Aufkleber auf Rückseite-> wo??? vermutete MAC Adresse:)
byte mac[] = {0x90, 0xA2, 0xDA, 0x00, 0x1B, 0x53};

// Eine IP im lokalen Netzwerk angeben. Dazu am besten die IP
// des PCs herausfinden (googlen!) und die letzte Zahl abändern
IPAddress ip(192, 168, 71, 3);

// Ethernet Library als Server initialisieren
// Verwendet die obige IP, Port ist per default 80
EthernetServer WebServer(80);



// Temperatur-Module ==========================================
// use hardware SPI, just pass in the CS pin
Adafruit_MAX31865 tempModule1 = Adafruit_MAX31865(SPI_CS_TEMP1);
Adafruit_MAX31865 tempModule2 = Adafruit_MAX31865(SPI_CS_TEMP2);
Adafruit_MAX31865 tempModule3 = Adafruit_MAX31865(SPI_CS_TEMP3);
Adafruit_MAX31865 tempModule4 = Adafruit_MAX31865(SPI_CS_TEMP4);

// The value of the Rref resistor. Use 430.0 for PT100 and 4300.0 for PT1000
#define RREF      4300.0
// The 'nominal' 0-degrees-C resistance of the sensor
// 100.0 for PT100, 1000.0 for PT1000
#define RNOMINAL  1000.0




// FRAM ==========================================
// FRAM Objekt initialisieren
Adafruit_FRAM_I2C framObj = Adafruit_FRAM_I2C();

// Fürs schreiben der Website ins FRAM: Aktuelle FRAM
// Adresse, wo das nächste Byte hin soll
uint16_t curFramAddress = 0;



// ====================================================================================
// Debug Funktionen

// in serielle Konsole ausgeben, wenn erwünscht.
void serialPrint(String msg) {
  Serial.println(msg);
}

// Debug-Funktion zum Anzeigen eines Arrays
void array2String(byte array[], unsigned int len, char buffer[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}






// ====================================================================================
// Zeit-Funktionen

// Es wird wenn möglich die externe RTC mit Batterie-Buffer verwendet,
// da diese mehrere Jahre läuft. Die interne läuft nur ca. 2 Wochen.
// Wenn die externe nicht verfügbar ist, wird die interne verwendet.

// Integer Wert in String umwandeln, führende Nullen ergänzen
// wenn nötig.
String addLZeros(int val, unsigned int numDigits) {
  String outStr = String(val);
  while(outStr.length() < numDigits) {
    outStr = '0' + outStr;
  }
  return outStr;
}




// Ermitteln, ob die externe RTC verfügbar ist
// zum beurteilen, ob die externe RTC läuft, eignet sich dieses Kriterium am ehesten...
bool extRtcRunning(DateTime rtcNow) {
  return rtcNow.year() > 2020;
}

// Timestamp zurückgeben als string in der Form 2023-08-31T01:02:03
void getTimestamp(char *buf) {
  DateTime rtcNow = rtcClock.now();
  if(extRtcRunning(rtcNow)) {
    rtcNow.timestamp(DateTime::TIMESTAMP_FULL).toCharArray(buf, 20);

  } else {
    String tStr;
    tStr = addLZeros(Controllino_GetYear()+2000, 4) + '-';
    tStr += addLZeros(Controllino_GetMonth(), 2) + '-';
    tStr += addLZeros(Controllino_GetDay(), 2) + 'T';
    tStr += addLZeros(Controllino_GetHour(), 2) + ':';
    tStr += addLZeros(Controllino_GetMinute(), 2) + ':';
    tStr += addLZeros(Controllino_GetSecond(), 2);
    tStr.toCharArray(buf, 20);
  }
}

// Dezimalzeit zurückgeben als integer (stunden*100+minuten)
unsigned int getDecTime() {
  unsigned int decTime;

  DateTime rtcNow = rtcClock.now();
  if(extRtcRunning(rtcNow)) {
    decTime = rtcNow.hour()*100 + rtcNow.minute();  

  } else {
    decTime = Controllino_GetHour()*100 + Controllino_GetMinute();
  }
  
  return decTime;  
}

// Zeit setzen (wenn per web interface eingegeben)
void adjustTime(unsigned int hour, unsigned int minute) {
  DateTime rtcNow = rtcClock.now();
  if(extRtcRunning(rtcNow)) {
    rtcClock.adjust(DateTime(rtcNow.year(), rtcNow.month(), rtcNow.day(), hour, minute));  

  } else {
    // day, week, month, year, hour, minute, second
    Controllino_SetTimeDate(
      Controllino_GetDay(),
      Controllino_GetWeekDay(),
      Controllino_GetMonth(),
      Controllino_GetYear(),
      hour, minute, 0);
  }
}



// Real time clocks starten
void setupRtcClock() {
  // Controllino RTC starten
  Controllino_RTC_init(0);

  // Aktuelle Controllino Zeit (ohne externe RTC)
  char timeNow[20];
  getTimestamp(timeNow);
  serialPrint("Controllino time is: " + String(timeNow));  

  // Externe RTC starten
  if (rtcClock.begin()) {
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time  this sketch was compiled
    //rtcClock.adjust(DateTime(F(__DATE__), F(__TIME__)));    

    // Zeit für Controllino übernehmen
    // day, week, month, year, hour, minute, second
    DateTime rtcNow = rtcClock.now();     
    Controllino_SetTimeDate(
      rtcNow.day(), rtcNow.dayOfTheWeek(), rtcNow.month(), rtcNow.year()-2000,
      rtcNow.hour(), rtcNow.minute(), rtcNow.second());

    // Das hier wäre schön, macht aber irgendwie Probleme:
    //  char timeFormatBuf[] = "YYYY-MM-DD hh:mm:ss";
    //  Serial.println(rtcNow.toString(timeFormatBuf));

    // neue Controllion Zeit von RTC
    char timeNow[20];
    getTimestamp(timeNow);
    serialPrint("Controllino / RTC time is: " + String(timeNow));

  } else {
    serialPrint("!!! Couldn't find external RTC !!!");    
  }
}


// ====================================================================================
// EEPROM Zugriff

// Settings im EEPROM speichern
void eeSaveSettings() {
  EEPROM.put(EE_SETTINGS_ADDR, Settings);
  eeSettingsChanged = false;
}

// Settings aus dem EEPROM einlesen
void eeLoadSettings() {
  const byte hasDataIdentifier = 61;

  // EEPROM Einstellungen auslesen
  if(EEPROM.read(EE_HASDATA_ADDR) == hasDataIdentifier) {
    // Daten im EEPROM vorhanden
    serialPrint("Setting loaded from EEPROM.");
    EEPROM.get(EE_SETTINGS_ADDR, Settings);
    eeSettingsChanged = false;

  } else {
    // keine Daten vorhanden
    serialPrint("No setting found in EEPROM.");
    EEPROM.write(EE_HASDATA_ADDR, hasDataIdentifier);
    eeSaveSettings();
  }  
}



// ====================================================================================
// Daten Handling

// Pumpe ein- oder ausschalten
void switchPump(bool newPumpState) {
  if(newPumpState) {
    // Pumpe ein
    digitalWrite(PUMP_OUT_PIN, HIGH); 

  } else {
    // Pumpe aus
    digitalWrite(PUMP_OUT_PIN, LOW);
  }
  delay(100); 
}

  // >200°C -> sensor open
  // <-100°C -> sensor short
boolean temperatureValid(float temp) {
  return (temp >= -100 && temp <= 200);
}




void updateState() {
  // Controller Logik -------------------------------------
  unsigned int decTime = getDecTime();
  
  // initialisieren: neuer Zustand der Pumpe
  unsigned int newPumpState = StateData.pumpState;

  if (Settings.pumpMode == 0) {
    // Schalten manuell
    // nichts zu tun hier...

  } else if (Settings.pumpMode == 1) {
    // Schalten nach Zeit
    if(Settings.pumpOnTime < Settings.pumpOffTime) {
      // Betrieb Tagsüber
      newPumpState = (decTime >= Settings.pumpOnTime && decTime <= Settings.pumpOffTime);
    } else {
      // Betrieb in der Nacht
      newPumpState = (decTime >= Settings.pumpOnTime || decTime <= Settings.pumpOffTime);
    }

  } else if (Settings.pumpMode == 2) {
    // Schalten nach Temperatur
    // Differenztemperatur muss grösser sein als angegebene Schwelle
    newPumpState = (StateData.tempBack - StateData.tempFwd) >= Settings.deltaTemp;

    // Sonne muss scheinen
    newPumpState = newPumpState & StateData.sunState;

    // Ausschaltverzögerung berücksichtigen
    if(StateData.pumpState == 1 && newPumpState == 0) {
      // Pumpe ist ein, soll ausgeschaltet werden
      if(pumpOffDelayEnds == 0) {
        // Delay Zeit startet, noch nicht ausschalten.
        pumpOffDelayEnds = millis() + Settings.pumpOffDelay*60*1000;
        newPumpState = 0;

      } else if (millis() < pumpOffDelayEnds) {
        // delay-Zeit noch nicht vorbei, noch warten!
        newPumpState = 0;
      }

    } else {
      // Pumpe muss nicht ausgeschaltet werden.
      pumpOffDelayEnds = 0;
    }

    // Blockier-Zeit berücksichtigen
    bool lockTime = (decTime > Settings.lockStartTime || decTime < Settings.lockStopTime);
    newPumpState = newPumpState & !lockTime;
  }

  // Wenn die Pumpe geschaltet werden soll: ausführen
  if(newPumpState != StateData.pumpState) {    
    switchPump(newPumpState);    
  }
}


// Status aktualiseren: Temperaturwerte einlesen,
// Pumpe schalten wenn nötig usw.
// wird alle paar Sekunden aufgerufen
void readValues() {
  // LED DO0 ein
  digitalWrite(LED_ACTIVITY_PIN, HIGH);

  // Daten ermitteln -------------------------------------
  // Aktuelle Zeit
  getTimestamp(StateData.timeStamp);
  serialPrint("readValues: " + String(StateData.timeStamp));

  // Temperaturen ermitteln
  // StateData.tempFwd = random(200);
  // StateData.tempBack = random(200);
  // StateData.tempTank = random(200);
  // StateData.tempSun = random(200);
  StateData.tempFwd = tempModule1.temperature(RNOMINAL, RREF);
  StateData.tempBack = tempModule2.temperature(RNOMINAL, RREF);
  StateData.tempTank = tempModule3.temperature(RNOMINAL, RREF);
  StateData.tempSun = tempModule4.temperature(RNOMINAL, RREF);  

  // Temperatur ausgeben
  serialPrint("Temperatures: Fwd=" + String(StateData.tempFwd) +
    ", Back=" + String(StateData.tempBack) +
    ", Tank=" + String(StateData.tempTank) +
    ", Sun=" + String(StateData.tempSun));

  // Pumpe
  StateData.pumpState = digitalRead(PUMP_OUT_PIN);

  // Meldungen zurücksetzen
  StateData.message = "";


  // Scheint die Sonne?
  if(!temperatureValid(StateData.tempSun)) {
    // Keine Angaben: Sonne scheint "immer", womit dieses Kriterium nicht berücksichtigt wird
    StateData.sunState = 1;
    StateData.message = "Sun temperature sensor seems broken!";

  } else {
    // Angaben da
    StateData.sunState = (StateData.tempSun >= Settings.lockSunTemp);
  }

  // Forward und back Werte verfügbar?
  if(!temperatureValid(StateData.tempFwd) || !temperatureValid(StateData.tempBack)) {
    if(!temperatureValid(StateData.tempFwd)) {
      StateData.message += "Forward temperature sensor seems broken! ";
    }
    if(!temperatureValid(StateData.tempBack)) {
      StateData.message += "Backward temperature sensor seems broken! ";
    }
    if(Settings.pumpMode == 2) {
      Settings.pumpMode = 1;
      StateData.message += "Switching to time mode!";
      webSendSettings = true;
    }
  }  

  // LED DO0 aus
  digitalWrite(LED_ACTIVITY_PIN, LOW);    

  // Status neu aktualisieren
  updateState();
}






// ====================================================================================
// Daten von der Website

// vom Client empfangener Temperaturwert validieren
bool validateTemp(float tempVal) {
  return tempVal >= 0 && tempVal <= 100;

}

// vom Client empfangener Zeit-Wert (Dezimalzeit) validieren
bool validateTime(float timeVal) {
  return timeVal >= 0 && timeVal < 2400;
}

// Die real time clock stellen
void setRtcTime(float timeVal) {
  // Zeit in der Form hour*100+min
  int hour = floor(timeVal/100);
  int minute = round(timeVal) % 100;

  if(hour >= 0 && hour < 24) {
    if(minute >= 0 && minute < 60) {
      adjustTime(hour, minute);
    }
  }  
}

// Funktion zum übernehmen eines Werts, der vom web-client
// gesendet wurde
void setValue(String paramName, float paramValue) {
  serialPrint(paramName + ": " +  String(paramValue));

  if (paramName == "controllertime") {
    // Zeit einstellen
    setRtcTime(paramValue);

  } else if (paramName == "pumpmode") {
    // Pump mode setzen
    if(paramValue >= 0 && paramValue <= 2) {
      Settings.pumpMode = round(paramValue);
      eeSettingsChanged = true;
    }

  } else if (Settings.pumpMode == 0) {
    if (paramName == "pumpstate") {
      // Pumpe schalten
      switchPump(round(paramValue) > 0);
    }

  } else if (Settings.pumpMode == 1) {
    if (paramName == "pumpontime") {
      // Pumpe ein Zeit
      if(validateTime(paramValue)) {
        Settings.pumpOnTime = round(paramValue);
        eeSettingsChanged = true;
      }

    } else if (paramName == "pumpofftime") {
      // Pumpe aus Zeit
      if(validateTime(paramValue)) {
        Settings.pumpOffTime = round(paramValue);
        eeSettingsChanged = true;
      }

    }    

  } else if (Settings.pumpMode == 2) {
    if (paramName == "deltatemp") {
      // Temperaturdifferenz
      if(validateTemp(paramValue)) {
        Settings.deltaTemp = paramValue;
        eeSettingsChanged = true;
      }

    } else if (paramName == "pumpoffdelay") {
      // Sonnen-Temperatur für Sonne scheint
      if(validateTemp(paramValue)) {
        Settings.pumpOffDelay = paramValue;
        eeSettingsChanged = true;
      }

    } else if (paramName == "locksuntemp") {
      // Sonnen-Temperatur für Sonne scheint
      if(validateTemp(paramValue)) {
        Settings.lockSunTemp = paramValue;
        eeSettingsChanged = true;
      }

    } else if (paramName == "lockstarttime") {
      // Zeit für Pumpe aus
      if(validateTime(paramValue)) {
        Settings.lockStartTime = round(paramValue);
        eeSettingsChanged = true;
      }

    } else if (paramName == "lockstoptime") {
      // Zeit für Pumpe ein
      if(validateTime(paramValue)) {
        Settings.lockStopTime = round(paramValue);
        eeSettingsChanged = true;
      }
    }    
  }
}





// ====================================================================================
// Web Server

// Web Server starten
void setupWebServer() {
  // Ethernet Verbindung und Server starten
  Ethernet.begin(mac, ip);
  WebServer.begin();

  // IP Adresse als string
  IPAddress ip = Ethernet.localIP();
  String ipStr = String(ip[0]) + "." +  String(ip[1]) + "." +  String(ip[2]) + "." +  String(ip[3]); 
  serialPrint("Webserver started, IP = " + ipStr); 
}


// Home Seite an Client schicken
void sendHomePage(EthernetClient webClient, char * httpRequest) {
  // HTTP Header 200 an den Browser schicken
  webClient.println("HTTP/1.1 200 OK");
  // webClient.println("Content-Type: text/html");
  // webClient.println("Content-Length: 64");
  webClient.println("Content-Type: text/html; charset=UTF-8");
  webClient.println("Content-Encoding: gzip");
  webClient.println("Connection: close"); // Verbindung wird nach Antwort beendet
  webClient.println();  
  delay(10);

  // Anzahl zu lesende Bytes
  uint16_t pageLength;
  framObj.readObject(0x00, pageLength);
  serialPrint("Website bytes: " + String(pageLength));

  // Nächste aus dem FRAM gelesene Adresse
  // Die Anzahl zu lesende Bytes braucht die ersten beiden Bytes.
  uint16_t curAddr = 2;


  uint16_t blockLength = 512; // Bytes

  uint16_t numBlocks = pageLength / blockLength;
  if(pageLength % blockLength > 0) {
    numBlocks += 1;
  }

 // Buffer zum lesen aus dem FRAM
  uint8_t dataBuffer[blockLength];

  // Lesen in Blöcken zu 512 bytes
  for (uint8_t ix = 0; ix < numBlocks; ix++) {

    digitalWrite(LED_ETHERNET_ACTIVE_PIN, HIGH);

    // FRAM lesen
    framObj.read(curAddr, dataBuffer, sizeof(dataBuffer)); 

    // Adresse nächster Block
    curAddr += blockLength;

    // String mit 0 beenden
    digitalWrite(LED_ETHERNET_ACTIVE_PIN, LOW);

    // Buffer ausgeben an Web client
    webClient.write(dataBuffer, sizeof(dataBuffer));
  }

  delay(1);
  digitalWrite(LED_ETHERNET_ACTIVE_PIN, HIGH);
  webClient.println("");
}



// Ajax requests

// Ajax HTTP Header senden (für json)
void sendAjaxHeader(EthernetClient webClient) {
  webClient.println("HTTP/1.1 200 OK");
  webClient.println("Content-Type: application/json");
  webClient.println("Connection: close"); // Verbindung wird nach Antwort beendet
  webClient.println();  
}

// Ajax get-Anfrage bearbeiten: Daten usw. als JSON schicken
void ajaxGetData(EthernetClient webClient) {
  // Daten in JSON umwandeln
  DynamicJsonDocument doc(1024);

  // Zeit
  char timeNow[20];
  getTimestamp(timeNow);
  doc["time"] = timeNow;

  // Daten
  doc["data"]["timeStamp"] = StateData.timeStamp;
  doc["data"]["tempFwd"]   = StateData.tempFwd;
  doc["data"]["tempBack"] = StateData.tempBack;
  doc["data"]["tempTank"] = StateData.tempTank;
  doc["data"]["tempSun"] = StateData.tempSun;
  doc["data"]["pumpState"] = StateData.pumpState;  
  doc["data"]["sunState"] = StateData.sunState;

  // Settings, wenn nötig
  if(webSendSettings) {
    doc["settings"]["pumpMode"] = Settings.pumpMode;
    doc["settings"]["pumpOnTime"] = Settings.pumpOnTime;
    doc["settings"]["pumpOffTime"] = Settings.pumpOffTime;
    doc["settings"]["deltaTemp"] = Settings.deltaTemp;
    doc["settings"]["pumpOffDelay"] = Settings.pumpOffDelay;    
    doc["settings"]["lockSunTemp"] = Settings.lockSunTemp;
    doc["settings"]["lockStartTime"] = Settings.lockStartTime;
    doc["settings"]["lockStopTime"] = Settings.lockStopTime;
    webSendSettings = false;
  }

  // JSON erstellen
  String jsonStr;
  serializeJson(doc, jsonStr);

  // Daten senden
  sendAjaxHeader(webClient);
  webClient.println(jsonStr);
}

// Ajax Anfrage bearbeiten, bei der Werte gesetzt (übernommen) werden sollen
void ajaxSetData(EthernetClient webClient, char * httpRequest) {
  // Start und Ende der URI Parameter
  // ajaxsetdata&nocache=3.141595358210404065
  String httpReqStr = String(httpRequest);
  int ix1 = httpReqStr.indexOf("ajaxsetdata=1&");
  int ix2 = httpReqStr.indexOf(" ", ix1+14);

  if((ix1 >= 0) && (ix2 > 0)) {
    // Start und Ende gefunden, Teil extrahieren
    String urlParamString = httpReqStr.substring(ix1+14, ix2);
    // serialPrint(urlParamString);

    // Parameter einzeln abarbeiten
    unsigned int ixStart = 0;
    while(ixStart < urlParamString.length()) {
      // = und & suchen
      int ixEq = urlParamString.indexOf('=', ixStart);
      int ixAnd = urlParamString.indexOf('&', ixEq);

      if(ixEq > 0 ) {
        // = gefunden.
        if(ixAnd < 0) {
          // & nicht gefunden -> Ende des strings nehmen
          ixAnd = urlParamString.length();
        }

        // Parameter Name
        String paramName = urlParamString.substring(ixStart, ixEq);

        // Parameter Wert
        float paramValue = urlParamString.substring(ixEq+1, ixAnd).toFloat();

        // Übernehmen
        setValue(paramName, paramValue);

        // Startposition für nächste Suche
        ixStart = ixAnd + 1;
      }
    }

    // Einstellungen speichern, wenn sich was geändert hat
    if(eeSettingsChanged) {
      webSendSettings = true;
      eeSaveSettings();
    }

    // Status neu ermitteln
    delay(50);
    readValues();    
  }
  delay(50);

  ajaxGetData(webClient);
}

// Web Server Funktion abarbeiten: testen auf client, Anfrage beantworten...
void processWebServer() {
  // server.available() schaut, ob ein webClient verfügbar ist und Daten
  // an den Server schicken möchte. Gibt dann eine webClient-Objekt zurück,
  // sonst false
  EthernetClient webClient = WebServer.available();
  // Wenn es einen webClient gibt, dann...
  if (webClient) {
    //serialPrint("Neuer webClient");
    digitalWrite(LED_ETHERNET_ACTIVE_PIN, HIGH);

    // Jetzt solange Zeichen lesen, bis eine leere Zeile empfangen wurde
    // HTTP Requests enden immer mit einer leeren Zeile
    boolean currentLineIsBlank = true;

    // Flag zum ermitteln der ersten Zeile des Requests
    // so dass nur diese ausgegeben wird über den seriellen Port
    boolean isFirstRequestLine = true;

    // Variable für den reinkommenden HTTP Request. Es zeigt sich hier, dass die
    // Verwendung von String für sowas heikel ist und andere Variabeln beeinflussen
    // kann. Daher char, obwohl es mühsam ist...
    char httpRequest[251];

    // Aktuelle Position in httpRequest
    uint16_t posIx = 0;


    // Solange webClient verbunden
    while (webClient.connected()) {
      // webClient.available() gibt die Anzahl der Zeichen zurück, die zum Lesen
      // verfügbar sind
      if (webClient.available()) {
        // Ein Zeichen lesen und übernehmen
        char c = webClient.read();
        if( posIx < 250) {
          httpRequest[posIx] = c;
          posIx++;
        }        

        // In currentLineIsBlank merken wir uns, ob diese Zeile bisher leer war.
        // Wenn die Zeile leer ist und ein Zeilenwechsel (das \n) kommt,
        // dann ist die Anfrage zu Ende und wir können antworten
        if (c == '\n' && currentLineIsBlank) {
          // String Ende markieren
          httpRequest[posIx] = '\0';

          // ausgeben (gesamter Request)
          // serialPrint(httpRequest);

          // char in string umwandeln zur einfacheren Verarbeitung
          String httpReqStr = String(httpRequest);

          if (httpReqStr.indexOf("ajaxgetstate") >= 0 ) {
            // Daten und Status senden
            webSendSettings = true;
            ajaxGetData(webClient);
          } 
          else if (httpReqStr.indexOf("ajaxgetdata") >= 0 ) {
            // read switch state and analog input
            ajaxGetData(webClient);
          }
          else if (httpReqStr.indexOf("ajaxsetdata") >= 0 ) {
            // read switch state and analog input
            ajaxSetData(webClient, httpRequest);
          } 
          else {
            // Home Seite ausgeben            
            sendHomePage(webClient, httpRequest);
          }

          break;
        }

        if (c == '\n') {
          // Zeilenwechsel, also currentLineIsBlack erstmal auf True setzen
          currentLineIsBlank = true;

          // nur die erste Zeile des requests seriell ausgeben
          if(isFirstRequestLine) {
            String httpReqStr = String(httpRequest);
            serialPrint(httpReqStr.substring(0, posIx-1));
            isFirstRequestLine = false;
          }
        }
        else if (c != '\r') {
          // Zeile enthält Zeichen, also currentLineIsBlack auf False setzen 
          currentLineIsBlank = false;
        }
      }
    }

    // Kleine Pause
    delay(10);

    // Verbindung schliessen
    webClient.stop();
    //serialPrint("Verbindung mit webClient beendet.");
    digitalWrite(LED_ETHERNET_ACTIVE_PIN, LOW);
  }  

}


// ====================================================================================
// Setup

// Daten vom Serieport empfangen und ins FRAM übernehmen.
// So wird die Website vom PC ins FRAM übertragen, siehe auch
// python-script website2fram.py
void processSerialData() {
  while (Serial.available() > 0) {
    // Daten-Buffer
    uint8_t dataBuffer1[1024];

    // Daten lesen, Anzahl gelesene Bytes
    uint16_t numBytesRead = Serial.readBytes(dataBuffer1, sizeof(dataBuffer1));

    // Daten weiter ins FRAM
    framObj.write(curFramAddress, dataBuffer1, numBytesRead);

    // Nächste zu schreibende Adresse im FRAM
    curFramAddress += numBytesRead;

    // Stand zurückmelden
    serialPrint(String(numBytesRead) + " Bytes geschrieben. Adresse: " + String(curFramAddress));

    // Bereit für weitere Daten
    serialPrint("ready");

  }
}



// ====================================================================================
// Setup

// Setup Funktion
void setup() {
  // Kurze Verzögerung, sonst hängt er manchmal beim Aufstarten
  delay(300);

  // Serielle Kommunikation starten, damit wir auf dem Seriellen Monitor
  // die Debug-Ausgaben mitlesen können.
  Serial.begin(19200);
  // Wait for USB Serial
  unsigned long timeOut = millis() + 3000;
  while (!Serial) {
    delay(100);
    if (millis() > timeOut) break;
  }  
  serialPrint("Initializing...");
  delay(300);



  // in and out pins
  serialPrint("Setting pin modes...");
  pinMode(PUMP_OUT_PIN, OUTPUT);            // Pumpe Relais
  pinMode(LED_ACTIVITY_PIN, OUTPUT);        // Aktivität (blinkt bei jedem readValues Durchgang)
  pinMode(LED_ETHERNET_ACTIVE_PIN, OUTPUT); // Webserver Aktivität

 // nächste 6 Ausgänge zum Anzeigen des Fortschritts beim Aufstarten
  pinMode(LED_PROGRESS_STEP1, OUTPUT);
  pinMode(LED_PROGRESS_STEP2, OUTPUT);
  pinMode(LED_PROGRESS_STEP3, OUTPUT);
  pinMode(LED_PROGRESS_STEP4, OUTPUT);
  pinMode(LED_PROGRESS_STEP5, OUTPUT);
  pinMode(LED_PROGRESS_STEP6, OUTPUT);
  delay(100);

  // real time clock
  serialPrint("Starting Real time clock...");
  setupRtcClock();
  
  digitalWrite(LED_PROGRESS_STEP1, HIGH);
  delay(300);




  // Temperaturen 
  serialPrint("Adafruit MAX31865 PT1000 Sensors...");
  tempModule1.begin(MAX31865_2WIRE);  // set to 2WIRE or 4WIRE as necessary
  tempModule2.begin(MAX31865_2WIRE);
  tempModule3.begin(MAX31865_2WIRE);  
  tempModule4.begin(MAX31865_2WIRE);
  
  digitalWrite(LED_PROGRESS_STEP2, HIGH);
  delay(300);


  // Settings laden aus EEPROM
  serialPrint("Loading settings from EEPROM...");
  eeLoadSettings();
  
  digitalWrite(LED_PROGRESS_STEP3, HIGH);
  delay(300);


  // FRAM initialisieren (website Daten)
  serialPrint("I2C FRAM...");
  if (framObj.begin()) {  // you can stick the new i2c addr in here, e.g. begin(0x51);
    serialPrint("I2C FRAM available.");
  } else {
    serialPrint("!!! I2C FRAM not found !!!");
  }

  digitalWrite(LED_PROGRESS_STEP4, HIGH);
  delay(300);



  // Timer zum aktualisieren des states starten
  serialPrint("Starting update timer...");
  stateUpdateTimer.Every(UPDATE_INTERVAL, readValues);

  digitalWrite(LED_PROGRESS_STEP5, HIGH);
  delay(300);


  // Ethernet Verbindung und Server starten
  serialPrint("Starting webserver...");
  setupWebServer();

  digitalWrite(LED_PROGRESS_STEP6, HIGH);
  delay(300);



#if WEBSITE_TO_FRAM_MODE > 0
  serialPrint("WEBSITE TO FRAM MODE !!! no auto update of values and state!");
  serialPrint("ready");

# else
  // Setup fertig.
  digitalWrite(LED_PROGRESS_STEP1, LOW);
  digitalWrite(LED_PROGRESS_STEP2, LOW);
  digitalWrite(LED_PROGRESS_STEP3, LOW);
  digitalWrite(LED_PROGRESS_STEP4, LOW);
  digitalWrite(LED_PROGRESS_STEP5, LOW);
  digitalWrite(LED_PROGRESS_STEP6, LOW);  

  serialPrint("Setup done.");
#endif

}








// ====================================================================================
// Main Loop

// Main loop
void loop() {

#if WEBSITE_TO_FRAM_MODE > 0
  // Daten vom Serieport übernehmen. Das brauchts nur um die Website raufzuladen.
  // Dann muss aber stateUpdateTimer deaktiviert (obige Zeile auskommentiert) werden.
  processSerialData();

#else
  // State aktualisieren
  stateUpdateTimer.Update();
#endif

  // Web-Server abarbeiten (requests usw)
  processWebServer();

  delay(10);
}



