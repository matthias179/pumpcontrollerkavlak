# Script zum konvertieren von HTML / CSS / JS Websites für den Arduino Webserver

import array

limitLength = 2400

with open("index_compressed.html") as f1:
    # alles einlesen
    f1Lines = f1.readlines()

with open("index_mod.txt", "w") as f2:    
    secIndex = 0
    secLength = 0
    secLenArray = array.array('i')
    
    f2.write('#if CURRENTSECTION == 0' + "\n")
    
    
    # Loop über alle Zeilen
    for f1Line in f1Lines:
        newLine = f1Line.strip()
        secLength = secLength + len(newLine)
        
        newLine = newLine.replace('\\', '\\\\').strip()
        # Anführungszeichen ersetzen
        newLine = newLine.replace('"', '\\"').strip()
        # secLength = secLength + len(newLine)
        # cpp Code ergänzen
        newLine = '"' + newLine + '"'        
        # in neue Datei schreibe
        f2.write(newLine + "\n")
        
        if secLength > limitLength:
            f2.write('#endif' + "\n")
            f2.write("\n")
            f2.write('#if CURRENTSECTION == ' + str(secIndex) + "\n")
            print(secLength)
            secLenArray.append(secLength)
            secLength = 0
            secIndex += 1
    
    f2.write('#endif' + "\n")
    secLenArray.append(secLength)
    print(secLength)
    
    numCharStr = ','.join(map(str, secLenArray))
    
    f2.write('uint16_t numChars[] = {' + numCharStr + '};' + "\n")
    
    print(sum(x for x in secLenArray))