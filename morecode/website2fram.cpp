


#include <Controllino.h>
#include "Adafruit_FRAM_I2C.h"

// FRAM ==========================================
Adafruit_FRAM_I2C framObj = Adafruit_FRAM_I2C();



// Setup Funktion
void setup() {
  // Serielle Kommunikation starten, damit wir auf dem Seriellen Monitor
  // die Debug-Ausgaben mitlesen können.
  Serial.begin(19200);
  // Wait for USB Serial
  unsigned long timeOut = millis() + 3000;
  while (!Serial) {
    delay(10);
    if (millis() > timeOut) break;
  }  
  Serial.println("Initializing...");
  delay(300);




  #define CURRENTSECTION 4

char myString[] = 
#if CURRENTSECTION == 0
"<!DOCTYPE HTML>"
"<html lang=en>"
"<head>"
"<meta name=viewport content=\"width=device-width, initial-scale=1.0\">"
"<link rel=icon href=data:,>"
"<title>Pump controller</title>"
"<script>document.addEventListener(\"DOMContentLoaded\",()=>{window.setInterval(function(){sendRequest(\"ajaxgetdata=1\");},5000);});function commitTimeModeValues(){const urlParams={pumpontime:readTimeValue('pumpOnTime'),pumpofftime:readTimeValue('pumpOffTime'),};commitValues(urlParams);}"
"function commitTempModeValues(){const urlParams={deltatemp:readTempValue('deltaTemp'),pumpoffdelay:readTempValue('pumpOffDelay'),locksuntemp:readTempValue('lockSunTemp'),lockstarttime:readTimeValue('lockStartTime'),lockstoptime:readTimeValue('lockStopTime'),};commitValues(urlParams);}"
"function commitTime(){const urlParams={controllertime:readTimeValue('controllerTime'),};commitValues(urlParams);}"
"function commitValues(urlParam){if(!urlParam){console.warn('commitValues: no parameter!');return;}"
"if(typeof urlParam==='object'){let valuesOk=true;for(let key in urlParam){valuesOk&=(urlParam[key]!==null)}"
"if(!valuesOk){console.warn('commitValues: parameter = null found!');return;}"
"const searchParams=new URLSearchParams(urlParam);urlParam=searchParams.toString();}"
"if(urlParam[0]!=='&'){urlParam='&'+urlParam;}"
"sendRequest(\"ajaxsetdata=1\"+urlParam);}"
"function sendRequest(urlParam){var nocache=\"&nocache=\"+Math.random()*10;console.log(urlParam+nocache);updateControls(JSON.stringify(simulateServerResponse()));return;var request=new XMLHttpRequest();request.onreadystatechange=function(){if(this.readyState==4){if(this.status==200){if(this.responseText!=null){updateControls(this.responseText);}}}}"
"request.open(\"GET\",urlParam+nocache,true);request.send(null);}"
"function updateControls(responseStr){console.log(responseStr)"
"try{dataObj=JSON.parse(responseStr);}catch(error){console.error(error);return;}"
"if('time'in dataObj){document.getElementById('controllerTime').value=formatTimeStamp(dataObj['time']);}"
"if('data'in dataObj){if('timeStamp'in dataObj['data']){document.getElementById('clock').textContent=formatTimeStamp(dataObj['data']['timeStamp']);}"
"if('tempFwd'in dataObj['data']){document.getElementById('tempFwd').textContent=formatTempValue(dataObj['data']['tempFwd']);}"
"if('tempBack'in dataObj['data']){document.getElementById('tempBack').textContent=formatTempValue(dataObj['data']['tempBack']);}"
"if('tempTank'in dataObj['data']){document.getElementById('tempTank').textContent=formatTempValue(dataObj['data']['tempTank']);}"
#endif

#if CURRENTSECTION == 1
"if('tempSun'in dataObj['data']){document.getElementById('tempSun').textContent=formatTempValue(dataObj['data']['tempSun']);}"
"if('pumpState'in dataObj['data']){if(dataObj['data']['pumpState']){document.getElementById('pumpState').setAttribute(\"fill\",\"#1f3\");}else{document.getElementById('pumpState').setAttribute(\"fill\",\"#eee\");}}"
"if('sunState'in dataObj['data']){if(dataObj['data']['sunState']){document.getElementById('sunState').setAttribute(\"fill\",\"#f90\");document.getElementById('sunState').setAttribute(\"stroke\",\"#f70\");}else{document.getElementById('sunState').setAttribute(\"fill\",\"#eee\");document.getElementById('sunState').setAttribute(\"stroke\",\"#ccc\");}}}"
"if('settings'in dataObj){if('pumpMode'in dataObj['settings']){setPumpMode(dataObj['settings']['pumpMode']);}"
"if('pumpOnTime'in dataObj['settings']){document.getElementById('pumpOnTime').value=formatTimeValue(dataObj['settings']['pumpOnTime']);}"
"if('pumpOffTime'in dataObj['settings']){document.getElementById('pumpOffTime').value=formatTimeValue(dataObj['settings']['pumpOffTime']);}"
"if('deltaTemp'in dataObj['settings']){document.getElementById('deltaTemp').value=formatTempIntValue(dataObj['settings']['deltaTemp']);}"
"if('pumpOffDelay'in dataObj['settings']){document.getElementById('pumpOffDelay').value=formatTempIntValue(dataObj['settings']['pumpOffDelay']);}"
"if('lockSunTemp'in dataObj['settings']){document.getElementById('lockSunTemp').value=formatTempIntValue(dataObj['settings']['lockSunTemp']);}"
"if('lockStartTime'in dataObj['settings']){document.getElementById('lockStartTime').value=formatTimeValue(dataObj['settings']['lockStartTime']);}"
"if('lockStopTime'in dataObj['settings']){document.getElementById('lockStopTime').value=formatTimeValue(dataObj['settings']['lockStopTime']);}}}"
"function formatTempValue(tempValue){return tempValue.toFixed(1).toString()+'\\u{00B0}C';}"
"function formatTempIntValue(tempValue){return Math.round(tempValue).toString();}"
"function formatTimeStamp(tStamp){var ix=tStamp.indexOf('T');if(ix>=0){return tStamp.substr(ix+1,5);}else{return'';}}"
"function formatTimeValue(timeValue){var hour=Math.floor(timeValue/100);var minute=timeValue-hour*100;var timeStr=Math.round(timeValue).toString();while(timeStr.length<4){timeStr='0'+timeStr;}"
"return timeStr[0]+timeStr[1]+\":\"+timeStr[2]+timeStr[3];}"
"function setPumpMode(pMode){for(let ix=0;ix<3;ix++){var ctrlBox=document.getElementById('pumpmode'+ix.toString());if(ix==pMode){ctrlBox.style.display='block';}else{ctrlBox.style.display='none';}}}"
#endif

#if CURRENTSECTION == 2
"function readTempValue(inputId){let inputObject=document.getElementById(inputId);let ctrlValue=inputObject.value.trim();let tempValue=parseInt(ctrlValue);if(isNaN(tempValue)){tempValue=null;}else if(tempValue<0||tempValue>100){tempValue=null;}"
"setInputBorder(inputObject,tempValue!==null);return tempValue;}"
"function readTimeValue(inputId){let inputObject=document.getElementById(inputId);let ctrlValue=inputObject.value.trim();let timeValue=null;timeParts=ctrlValue.match(/(\\d{1,2}):?(\\d{2})/);if(timeParts){let hour=parseInt(timeParts[1]);let minute=parseInt(timeParts[2]);if(hour>=0&&hour<24&&minute>=0&&minute<60){timeValue=hour*100+minute;}}"
"setInputBorder(inputObject,timeValue);return timeValue;}"
"function setInputBorder(inputObject,valueOk){if(valueOk){inputObject.style.borderColor='#bbb';inputObject.style.borderWidth=\"1px\";}else{inputObject.style.borderColor='#f00';inputObject.style.borderWidth=\"3px\";}}"
"function simulateServerResponse(){const curDate=new Date();let curDateStr=curDate.toISOString();var response={time:curDateStr,data:{timeStamp:curDateStr,tempFwd:Math.random()*50,tempBack:Math.random()*50,tempTank:Math.random()*50,tempSun:Math.random()*50,pumpState:Math.floor(Math.random()*2),sunState:Math.floor(Math.random()*2),},settings:{pumpMode:Math.floor(Math.random()*3),pumpOnTime:Math.floor(Math.random()*2400),pumpOffTime:Math.floor(Math.random()*2400),deltaTemp:Math.random()*50,pumpOffDelay:Math.round(Math.random()*10),lockSunTemp:Math.random()*50,lockStartTime:Math.floor(Math.random()*2400),lockStopTime:Math.floor(Math.random()*2400),}}"
"return response;}</script>"
"<style>*{font:14px sans-serif}main{width:350px;margin:0 auto;text-align:center}footer p{font-size:10px}h1{font-size:18px;margin-bottom:0;text-align:left}h2{font-size:16px}button{padding:6px 12px;margin-right:5px;min-width:90px}input{margin-bottom:1em;padding:6px 12px;width:60px;text-align:center;border:1px solid #bbb}label{display:inline-block;margin-right:.5em;width:180px;text-align:right}.ctrlbox{position:relative;margin:1em 0;padding-bottom:1em;border-bottom:1px solid #bbb}.ctrlbox p{text-align:left}.buttons{text-align:center}#clock{position:absolute;right:0}#pumpmode0,#pumpmode1,#pumpmode2{display:none}</style>"
"</head>"
"<body>"
"<main>"
"<div class=ctrlbox>"
"<span id=clock>12:34</span>"
"<h1>Pump controller</h1>"
"</div>"
"<svg width=300 height=200 version=1.1>"
"<g stroke=#000 stroke-width=2>"
"<g stroke=#06f>"
"<path d=\"m220 170h-170\"/>"
#endif

#if CURRENTSECTION == 3
"<path d=\"m50.5 170v-50\"/>"
"</g>"
"<path d=\"m100 59.5h120\" stroke=\"#f00\"/>"
"<circle id=pumpState cx=170 cy=170 r=19.5 fill=#fff />"
"<path d=\"m152 170 18-18v36z\" fill=none />"
"<g fill=#eef>"
"<rect x=220.5 y=50 width=69.5 height=130 />"
"<rect transform=rotate(40) x=111 y=-32 width=10 height=\"100\"/>"
"</g>"
"</g>"
"<g font-family=sans-serif>"
"<g font-size=16px font-weight=bold>"
"<text id=tempTank x=232 y=119 fill=#000>--</text>"
"<text id=tempFwd x=80 y=159 fill=#06f>--</text>"
"<text id=tempBack x=160 y=49 fill=#f00>--</text>"
"<text id=tempSun x=85 y=40 fill=#f70>--</text>"
"</g>"
"<g font-size=12px>"
"<text x=232 y=99 fill=#000>Boiler:</text>"
"<text x=70 y=139 fill=#06f>Forward:</text>"
"<text x=160 y=29 fill=#f00>Back:</text>"
"<text x=85 y=20 fill=#f70>Sun:</text>"
"</g>"
"</g>"
"<g id=sunState fill=#fff stroke=#f70>"
"<path transform=\"translate(25.5 -25)\" d=\"m20 110-2-32-21 24 17-27-31 7 30-12-30-12 31 7-17-27 20 24 2-32 2 32 21-24-17 27 31-7-30 12 30 12-31-7 17 27-20-24z\" />"
"<ellipse cx=45 cy=45 rx=15 ry=15 />"
"</g>"
"</svg>"
"<div class=ctrlbox>"
"<h2>Select operation mode:</h2>"
"<div class=buttons><button onclick=\"commitValues('pumpmode=0')\">Manual</button>"
"<button onclick=\"commitValues('pumpmode=1')\">Time</button>"
"<button onclick=\"commitValues('pumpmode=2')\">Temperature</button></div>"
"</div>"
"<div class=ctrlbox id=pumpmode0>"
"<h2>Manual mode active</h2>"
"<p>Pump is switched on / off manually:</p>"
"<div class=buttons><button onclick=\"commitValues('pumpstate=1')\">ON</button><button onclick=\"commitValues('pumpstate=0')\">OFF</button></div>"
"</div>"
"<div class=ctrlbox id=pumpmode1>"
"<h2>Time mode active</h2>"
"<p>Pump is switched on / off at these times:</p>"
"<label for=pumpOnTime>Pump ON time (HH:MM):</label><input type=text id=pumpOnTime><br>"
"<label for=pumpOffTime>Pump OFF time (HH:MM):</label><input type=text id=pumpOffTime><br>"
"<div class=buttons><button onclick=commitTimeModeValues()>Set</button></div>"
"</div>"
"<div class=ctrlbox id=pumpmode2>"
"<h2>Temperature mode active</h2>"
"<p>Pump is switched on / off considering measured temperatures</p>"
"<p>Operate pump when temperature difference between forward and back line is (or higher):</p>"
"<label for=deltaTemp>Temp. difference (&deg;C):</label><input type=text id=deltaTemp><br>"
"<p>When minimum temperature difference for switching off pump is reached, wait for this many minutes before switching pumo off:</p>"
"<label for=pumpOffDelay>Delay (minutes):</label><input type=text id=pumpOffDelay><br>"
"<p>Never operate pump when sun detector temperature (yellow) is below:</p>"
#endif

#if CURRENTSECTION == 4
"<label for=lockSunTemp>Temp. limit (&deg;C):</label><input type=text id=lockSunTemp><br>"
"<p>Never operate pump in this time span:</p>"
"<label for=lockStartTime>From (HH:MM):</label><input type=text id=lockStartTime><br>"
"<label for=lockStopTime>To (HH:MM):</label><input type=text id=lockStopTime><br>"
"<div class=buttons><button onclick=commitTempModeValues()>Set</button></div>"
"</div>"
"<div class=ctrlbox>"
"<a href=# onclick=\"document.getElementById('adjCtrlTime').style.display='block';return false\">Adjust controller time</a>"
"<div id=adjCtrlTime style=display:none>"
"<h2>Adjust controller time</h2>"
"<label for=controllerTime>Set time to (HH:MM):</label><input type=text id=controllerTime><br>"
"<div class=buttons><button onclick=\"document.getElementById('adjCtrlTime').style.display='none';commitTime()\">Set</button></div>"
"</div>"
"</div>"
"<footer>"
"<p>Version 1.0, 2023, Matthias Glatt</p>"
"</footer>"
"</main>"
"</body>"
"</html>"
#endif
"";

  uint16_t numChars[] = {2467, 2478, 2415, 2446, 898}; // 10704


    // Verbindung zum FRAM
  Serial.println("I2C FRAM...");
  if (framObj.begin()) {  // you can stick the new i2c addr in here, e.g. begin(0x51);
    Serial.println("I2C FRAM available.");
  } else {
    Serial.println("!!! I2C FRAM not found !!!");
  }
  
  // Start Adresse fürs Schreiben
  uint16_t startAddr = 0;
  for (int ix = 0; ix < CURRENTSECTION; ix++) {
    startAddr += numChars[ix];
  }

    // Daten schreiben
  Serial.print("writing string: ");
  Serial.print(strlen(myString));
  Serial.println(" bytes...");

  framObj.write(startAddr, (uint8_t*)myString, strlen(myString));



    // Zur Kontrolle jeweils 150 bytes lesen
  Serial.println("reading 150bytes...");
  startAddr = 0;
  for (int ix = 0; ix <= CURRENTSECTION; ix++) {
    uint8_t s2[151];
    framObj.read(startAddr, s2, 150); 
    s2[150] = '\0';

    Serial.println((char*)s2);  

    startAddr += numChars[ix];
  }

    // und fertig.
  Serial.println("done.");
}


void loop() {
}
