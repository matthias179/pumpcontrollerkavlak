# Script zum komprimieren der Website und erstellen der
# website.h Datei, die der Arduino Compiler reinladen kann
# 2023-10-07, Matthias Glatt

import gzip

# ==================================================================
# Einstellungen

# Name der Website Datei
websiteFile = 'index.html'

outFile = 'website.h'



# ==================================================================
# Datei einlesen und bearbeiten

# Datei einlesen
with open(websiteFile) as fid:
    fileContents = fid.readlines()

# Flag für Testcode
isTestCode = False

# Leerzeichen vorne und hinten weg
for index, line in enumerate(fileContents):
    fileContents[index] = fileContents[index].strip()
    
    # Was als Testcode markiert ist: weglassen
    if(fileContents[index] == '// TESTCODE START'):
        # Testcode Anfang
        fileContents[index] = ''
        isTestCode = True
        
    elif(fileContents[index] == '// TESTCODE END'):
        # Testcode Ende
        fileContents[index] = ''
        isTestCode = False
    
    elif(isTestCode):
        # Testcode: weglassen
        fileContents[index] = ''
        
    else:
        # Kommentare weglassen
        if(fileContents[index][:2] == '//'): # Javascript Kommentar
            fileContents[index] = ''
            
        elif(fileContents[index][:4] == '<!--'): # HTML Kommentar
            fileContents[index] = ''

# leere Zeilen weg
fileContents = list(filter(None, fileContents))

# Zeilenumbrüche
fileContents = "\n".join(fileContents)
# print(fileContents)
# print(len(fileContents))


# ==================================================================
# Website komprimieren

# Website komprimieren
compr = gzip.compress(fileContents.encode())
listByteWebsite = list(compr)

# Länge ausgeben
print("Komprimierte Website Länge: %d Bytes" % len(listByteWebsite))

# in h-Datei schreiben
with open(outFile, 'w') as f:
    f.write('const byte PROGMEM website[%d] = {0x' % len(listByteWebsite))
    f.write(',0x'.join('{:02x}'.format(x) for x in listByteWebsite))
    f.write('};')
    
    
