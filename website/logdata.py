
# Script zum sampeln von Daten vom Controller in eine csv-Datei
# Starten: python3 logdata.py
#
# 2024-01-02, Matthias Glatt

import time
import os.path
import sys
import threading
import requests
import csv
from datetime import datetime




# URL, wo die Daten zu holen sind
dataUrl = 'http://192.168.71.3/?ajaxgetstate=1'

# Sampling Intervall in Sekunden
sampleInterval = 30.0



# Flag fürs stoppen des Threads
keepRunning = True

# Funktion zum abrufen der Daten und speichern in eine csv-Datei
def sampleData():
    # soll gestoppt werden?
    if not keepRunning:
        sys.exit(0)
        
    # Timer: alle n Sekunden werden Daten gesampelt
    threading.Timer(sampleInterval, sampleData).start()
    
    # Start-Zeit des sample-Prozesses ausgeben
    print(f'{datetime.now():%Y-%m-%d  %H:%M:%S}')
    
    # Send a GET request to the desired API URL
    response = requests.get(dataUrl)


    # If everything went well, parse the response
    # and print it
    if response.status_code == 200:
        data = response.json()
        #print(data)
    else:
        # Print an error message
        print('HTTP ' + response.status_code + '!!! Error fetching data!!!')
        return
    

    # Dateiname der Daten Log-Datei
    dataFileName = 'logdata' + f'{datetime.now():%Y-%m-%d}' + '.csv'

    # Wenn die Datei noch nicht existiert: Header schreiben
    if(not os.path.exists(dataFileName)):
        with open(dataFileName, 'w', newline='') as fid:
            writer = csv.writer(fid)
            writer.writerow(['timeStamp', 'tempFwd', 'tempBack', 'tempTank', 'tempSun', 'pumpState', 'sunState', 
                'pumpMode', 'pumpOnTime', 'pumpOffTime', 'deltaTemp', 'pumpOffDelay', 'lockSunTemp', 'lockStartTime', 'lockStopTime'])

    # Daten schreiben
    with open(dataFileName, 'a', newline='') as fid:
        writer = csv.writer(fid)
        writer.writerow([
            data['data']['timeStamp'],
            data['data']['tempFwd'],
            data['data']['tempBack'],
            data['data']['tempTank'],
            data['data']['tempSun'],
            data['data']['pumpState'],
            data['data']['sunState'],
            data['settings']['pumpMode'],
            data['settings']['pumpOnTime'],
            data['settings']['pumpOffTime'],
            data['settings']['deltaTemp'],
            data['settings']['pumpOffDelay'],
            data['settings']['lockSunTemp'],
            data['settings']['lockStartTime'],
            data['settings']['lockStopTime']])    
        
        
        
if __name__ == "__main__":
    
    try:
        # Sampling starten
        sampleData()
        print("Sampling. Press ctrl-c to stop")

        while True:
            # sampling läuft, warten
            time.sleep(1)
            
    except KeyboardInterrupt: 
        # ctrl-c gedrückt
        keepRunning = False
        print("ctrl-c pressed, stopping thread...")
        sys.exit(0) 
        
       
        
    