# Script zum Übertragen der Website ins FRAM des Controllino, so dass
# sie von dort aus per Controllino Webserver verwendet werden kann.
# Die Website (html-Datei) wird komprimiert und dann blockweise
# über die serielle Verbindung an den Controllino geschickt.
# 2023-10-07, Matthias Glatt

import gzip
import serial

# ==================================================================
# Einstellungen

# Name der Website Datei
websiteFile = 'index.html'

# Serieller Port mit dem Arduino
serialPort = "/dev/ttyACM0"

# Baudrate serieller Port
baudRate = 19200

# Anzahl Bytes zu senden aufs mal
bytesPerBlock = 512



# ==================================================================
# Datei einlesen und bearbeiten

# Datei einlesen
with open(websiteFile) as fid:
    fileContents = fid.readlines()

# Flag für Testcode
isTestCode = False

# Leerzeichen vorne und hinten weg
for index, line in enumerate(fileContents):
    fileContents[index] = fileContents[index].strip()
    
    # Was als Testcode markiert ist: weglassen
    if(fileContents[index] == '// TESTCODE START'):
        # Testcode Anfang
        fileContents[index] = ''
        isTestCode = True
        
    elif(fileContents[index] == '// TESTCODE END'):
        # Testcode Ende
        fileContents[index] = ''
        isTestCode = False
    
    elif(isTestCode):
        # Testcode: weglassen
        fileContents[index] = ''
        
    else:
        # Kommentare weglassen
        if(fileContents[index][:2] == '//'): # Javascript Kommentar
            fileContents[index] = ''
            
        elif(fileContents[index][:4] == '<!--'): # HTML Kommentar
            fileContents[index] = ''

# leere Zeilen weg
fileContents = list(filter(None, fileContents))

# Zeilenumbrüche
fileContents = "\n".join(fileContents)
# print(fileContents)
# print(len(fileContents))


# ==================================================================
# Website komprimieren

# Website komprimieren
compr = gzip.compress(fileContents.encode())

# Länge der komprimierten Daten in Bytes
lengthBytes = len(compr).to_bytes(2, 'little')

# am Anfang die Länge in Bytes anhängen
compr = lengthBytes + compr 
listByteWebsite = list(compr)

# Länge ausgeben
print("Komprimierte Website Länge: %d Bytes" % len(listByteWebsite))



# ==================================================================
# An den Arduino / Controllino schicken

# Serieller Port initialisieren
serialPort = serial.Serial(
    port=serialPort, baudrate=baudRate, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE
)

# Used to hold data coming over UART
serialIn = ""  

# Anzahl bereits gesendete Bytes
bytesSent = 0

# Flag für Ende-Meldung (Damit diese nur einmal angezeigt wird...)
endMessageShown = False

try:
    while 1:
        # Wait until there is data waiting in the serial buffer
        if serialPort.in_waiting > 0:

            # Read data out of the buffer until a carraige return / new line is found
            serialIn = serialPort.readline()
            
            # umwandeln in ascii string
            try:
                serialStr = serialIn.decode("Ascii").strip()
                print("> " + serialStr)
            except:
                pass
            
            # Controller meldet ready, wenn er Daten annehmen kann
            if serialStr[:5] == 'ready':
                # Wenn noch Daten zum schreiben sind
                if bytesSent < len(listByteWebsite):
                    serialPort.write(listByteWebsite[bytesSent:min(bytesSent+bytesPerBlock, len(listByteWebsite))])
                    bytesSent += bytesPerBlock
                    
                elif not endMessageShown:
                    # Fertig, aber Ende-Meldung noch nicht angezeigt
                    print("Übertragung abgeschlossen. Terminal mit CTRL-c beenden.")

                
except KeyboardInterrupt:
    # User interrupt the program with ctrl+c
    exit()                
                


